from twitchio.ext.commands.bot import Bot
from twitchio.ext import commands

from constants.app import (
    TMI_TOKEN,
    CLIENT_ID,
    BOT_NICK,
    BOT_PREFIX,
    CHANNEL
)

from chat_constants import CHAT_WORDS


class ChatBot(Bot):

    def __init__(self):
        super().__init__(irc_token=TMI_TOKEN,
                         client_id=CLIENT_ID,
                         nick=BOT_NICK,
                         prefix=BOT_PREFIX,
                         initial_channels=[CHANNEL])

    async def event_ready(self):
        print("{} is here!".format(BOT_NICK))
        ws = self._ws
        await ws.send_privmsg(CHANNEL, f"/me has arrived!")

    async def event_message(self, ctx):
        if ctx.author.name.lower() == BOT_NICK.lower():
            return
        await self.handle_commands(ctx)

        [await ctx.channel.send(f"Hi, @{ctx.author.name}!")
         for word in CHAT_WORDS if ctx.content.lower() == word]

    @commands.command(name="commands")
    async def commands_overview(self, ctx):
        await ctx.send("!commands, !WR, !бан, !анбан")

    @commands.command(name="WR")
    async def w_r(self, ctx):
        await ctx.send("https://www.speedrun.com/masseffect1/run/yl021l3y")

    @commands.command(name="бан")
    async def ban_joke(self, ctx):
        await ctx.send(f"/me {ctx.author.name} банит {ctx.message.content[4:]}")

    @commands.command(name="анбан")
    async def unban_joke(self, ctx):
        await ctx.send(f"/me {ctx.author.name} разбанивает {ctx.message.content[6:]}")


bot = ChatBot()
bot.run()

